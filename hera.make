api = 2
core = 7.x

; For Drupal 7:
projects[drupal][type] = "core"
projects[drupal][download][type] = "git"
projects[drupal][download][url] = "https://github.com/omega8cc/7x.git"
projects[drupal][download][tag] = "7.34.1"

; Download the HERA install profile and recursively build all its dependencies:
projects[hera][type] = profile
projects[hera][download][type] = "git"
projects[hera][download][url] = "git@bitbucket.org:heraio/distro.git"
