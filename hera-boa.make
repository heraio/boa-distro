api = 2
core = 7.x

; For Drupal 7:
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "http://files.aegir.cc/core/drupal-7.33.1.tar.gz"

; Download the HERA install profile and recursively build all its dependencies:
projects[hera][type] = profile
projects[hera][download][type] = "git"
projects[hera][download][url] = "git@bitbucket.org:heraio/distro.git"
